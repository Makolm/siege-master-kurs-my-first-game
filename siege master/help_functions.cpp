#include "help_functions.h"

namespace hf {

	double sum = 0., multi = -1., sub=0.;

	double extent(double x, unsigned limit) {

		double sub = x;

		if (!limit) return 1.;

		for (unsigned i = 1; i <= limit; i++) sub *= x;

		return sub;

	}

	unsigned fact(unsigned limit) {

		if (!limit) return 0;

		sub = 1;

		for (unsigned i = 1; i <= limit; i++)sub *= i;

		return sub;
		
	}

	double sinus(double x, unsigned limit) {

		for (unsigned i = 0; i < limit; i++)
			sum += ((-1 * multi) * extent(x, 2 * i + 1)) / fact(2 * i + 1);

		return sum;

	}

	

}