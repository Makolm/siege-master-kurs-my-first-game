#pragma once

#include <SFML/Graphics.hpp>
#include "map.h"
#include "settings.h"

using namespace standart_std;

class artillery {

protected:

	struct formul {

		Vector2f coord;
		IntRect w_h;
		int block_x1 = 0, block_x2 = 0;
		int block_y1 = 0, block_y2 = 0, block_y3 = 0;
		double x = 0, y = 0,degree = 0.;

	};

	float x, y, rotate, speed, min_rotate,max_rotate, origin_lever;

	unsigned missiles, attack_power;

	static bool unlimited_patrons;

	Texture texture_base, texture_lever;

	Sprite base, lever;

	bool is_air(map&, formul&);

	virtual bool control_back(map& map_, formul& formul_) { return true; }

	virtual bool control_back_hit(map& map_, formul& formul_) { return false; }


public:

	enum art {

		_ballista,
		_catapult,
		_trebuchet,

	};

	artillery(float x = 2.f* map::block_size, float y = (map::height - 4.f) * map::block_size);

	void set_missiles(unsigned sub) { missiles = sub; }

	unsigned get_missiles() { return missiles; }

	static void set_limit(bool limit) { unlimited_patrons = limit; }

	static	bool get_limit() { return unlimited_patrons; }

	virtual void show(RenderWindow&)=0;

	virtual void attack(RenderWindow&, all_controls&, map&)=0;

};

//������� ����������
class near_field_artillery:public artillery {

protected:

	Texture texture_arrow;

	Sprite arrow;

	virtual void on_attack(formul&) = 0;

	bool pos_arrow(RenderWindow&,map& map_, formul&);

	virtual void to_origin() = 0;

public:

	near_field_artillery(float x = 2.f * map::block_size, float y = (map::height - 4.f) * map::block_size);

	void attack(RenderWindow&, all_controls&, map&);


};

class ballista : public near_field_artillery {

	float sin_arrow, cos_arrow;
	Texture texture_stand;

	Sprite stand;

	void on_attack(formul&);

	void to_origin();

public:

	enum position_ballista {

		base_x = 67,
		base_y = -10,
		base_origin_x = 66,
		base_origin_y = 15,

		stand_x = 15,
		stand_y = -15,

		arrow_x = 90,
		arrow_y = -15,

		lever_x = 68,
		lever_y = -8,

	};

	ballista(float x = 2.f * map::block_size, float y = (map::height - 4.f) * map::block_size);

	void show(RenderWindow&);

	ballista& aiming_at(float);

	ballista& operator+=(float);

	ballista& operator-=(float);

};

//������� ����������
class far_field_artillery : public artillery {

protected:

	Texture texture_dipper, texture_stone;
	
	Sprite dipper, stone;

	virtual void sprites(float angle) = 0;

	virtual void on_attack(formul&)=0;

	virtual void to_origin()=0;

	bool pos_stone(map&, formul&);

	bool control_back(map& map_, formul& formul_);

	bool control_back_hit(map&, formul&);
	

public:

	far_field_artillery(float x = 2.f * map::block_size, float y = (map::height - 4.f) * map::block_size);

	void attack(RenderWindow&, all_controls&,map&);

	void aiming_at(float);

};
 
class catapult : public far_field_artillery {

	float distation, max_distation;

	Texture texture_wheel;

	Sprite wheel_1, wheel_2;

	enum position_catapult {


		base_y = 10,

		dipper_x = 42,
		dipper_y = 25,
		origin_dipper_x = 70,
		origin_dipper_y = 65,

		wheel_1_x = 5,
		wheel_y = 67,
		wheel_2_x = 89,
		wheel_origin = 15,

		lever_x = 46,
		lever_y = 67,

		stone_x=50,
		stone_y=24,
		attack_x = 5,
		attack_y = -65

	};

	void sprites(float);

	void on_attack(formul&);

	void to_origin();

public:

	catapult(float x = 2.f * map::block_size, float y = (map::height - 4.f) * map::block_size);

	void show(RenderWindow&);

	catapult& move(float);
	
	catapult& go_in(float,float);

	catapult& set_max_distation(float dist);

	catapult& operator +=(float);

	catapult& operator -=(float);

};

class trebuchet : public far_field_artillery {

	enum position_trebuchet {

		base_y = -10,

		dipper_x =  87,
		dipper_y = 13,

		dipper_origin_x =107,
		dipper_origin_y = 15,

		lever_x=88,
		lever_y=18,

		stone_x = 92,
		stone_y = 2,
		attack_x = 45,
		attack_y = -95,

	};

	void sprites(float);
	
	void on_attack(formul&);

	void to_origin();

public:

	trebuchet(float x = 2.f * map::block_size, float y = (map::height-4.f) * map::block_size);

	void show(RenderWindow&);

	trebuchet& operator+=(float);

	trebuchet& operator-=(float);

};