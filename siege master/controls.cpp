#include <SFML/Graphics.hpp>
#include "controls.h"
#include "artillery.h"
#include "map.h"
#include "settings.h"

#include <iostream>

#define key Keyboard
#define mouse_is Mouse::isButtonPressed
#define key_is Keyboard::isKeyPressed
#define mouse_pressed Event::MouseButtonPressed

ballista ballista_;
catapult catapult_;
trebuchet trebuchet_;

all_controls::all_controls() {
	
	time = 0;
	choose_artillery = artillery::_ballista;
	evt.type = Event::Count;
	choose_block = grass;
	win_or_lose = redactor_mode = repaint = false;
	new_missiles = frame_icon = true;

}

void show(RenderWindow& wnd, all_controls& control, map& map_){;

	wnd.clear();
	map_.draw(wnd);

	if (control.new_missiles) {

		ballista_.set_missiles(map_.get_for_wins());
		catapult_.set_missiles(map_.get_for_wins());
		trebuchet_.set_missiles(map_.get_for_wins());

		control.new_missiles = false;

	}

	if(!control.redactor_mode){ 

		if (control.choose_artillery == artillery::_ballista)
			show_artillery(wnd, ballista_);

		else if (control.choose_artillery == artillery::_catapult)
			show_artillery(wnd, catapult_);

		else if (control.choose_artillery == artillery::_trebuchet)
			show_artillery(wnd, trebuchet_);

		if(control.frame_icon)control.frame.show(wnd);

	}
	
	wnd.display();

}

//������� ��� ��������
void control_events(RenderWindow& wnd, all_controls& control, map& map_) {

	if (control.evt.type == Event::Closed) wnd.close();

	//���/���� ���������, ���������, ����� ���������
	else if (control.evt.type == Event::KeyPressed) {

		if ((control.time + 300) > clock())return;

		else if (key_is(key::R) && !control.redactor_mode) {

			control.redactor_mode = control.repaint = true;

			cout << "redactor mode: on\nf5 == save map\nf7 == load map\n";
			cout <<	"left click == block\nright click == air\n\q and r == set block\n";

		}

		else if (key_is(key::R) && control.redactor_mode) {

			control.redactor_mode = false;
			
			control.repaint = true;

			cout << "redactor mode: off\n";

		}

		else if (key_is(key::D) && !map_.get_mode()) {

			map_.set_mode(true);

			cout << "destructor mode on\n";

		}

		else if (key_is(key::D) && map_.get_mode()) {

			map_.set_mode(false);

			cout << "destructor mode off\n";

		}

		else if (key_is(key::U) && !artillery::get_limit()) {

			artillery::set_limit(true);

			cout << "unlimited mode on\n";

		}

		else if (key_is(key::U) && artillery::get_limit()) {

			artillery::set_limit(false);

			cout << "unlimited mode off\n";

		}

		else if (key_is(key::H)) { help_me(); }

		else if (control.redactor_mode) {

			if (key_is(key::Q)) {

				if (control.choose_block <= dirt) { cout << "this is min limit of block\n"; return; }

				control.choose_block--;

			}
		

			else if (key_is(key::E)) {

				if (control.choose_block >= target_reverse) { cout << "this is max limit of block\n"; return; }

				control.choose_block++;

			}

			if (key_is(key::Q) || key_is(key::E)) {

				if (control.choose_block == dirt) cout << "choose: dirt\n";
				else if (control.choose_block == grass) cout << "choose: grass\n";
				else if (control.choose_block == brick) cout << "choose: brick\n";
				else if (control.choose_block == target) cout << "choose: target\n";
				else if (control.choose_block == target_reverse) cout << "chose: target_reverse\n";

			}

			else if (key_is(key::F5)) { map_.save(); cout << "map is save\n"; }

			else if (key_is(key::F7)) { 
				
				map_.load(); 
				cout << "map is load\n";
				control.new_missiles = control.repaint = true;
			
			}

		}

		else if (!control.redactor_mode) {

			if (key_is(key::Num1)) {

				control.choose_artillery = artillery::_ballista;
				control.repaint = true;
				cout << "choose: ballista\n";

			}

			else if (key_is(key::Num2)) {

				control.choose_artillery = artillery::_catapult;
				control.repaint = true;
				cout << "choose: catapult\n";

			}

			else if (key_is(key::Num3)) {

				control.choose_artillery = artillery::_trebuchet;
				control.repaint = true;
				cout << "choose: trebuchet\n";
			
			}

			else if (key_is(key::F) && control.frame_icon) { control.frame_icon = false; cout << "frame was close\n"; }

			else if (key_is(key::F) && !control.frame_icon) { control.frame_icon = true; cout << "frame was open\n"; }

		}

		if (control.repaint) { show(wnd, control, map_); control.repaint = false; }
		
		control.time = clock();

	}

	//��������
	else if (control.redactor_mode && control.evt.type == mouse_pressed) {

		int prev_choose;

		while (mouse_is(Mouse::Left) || mouse_is(Mouse::Right)) {

			prev_choose = control.choose_block;

			if (mouse_is(Mouse::Right))
				control.choose_block = air;

			map_.redactor(wnd, control);
			show(wnd, control, map_);

			control.choose_block = prev_choose;

		}

	}

}

void all_animations(RenderWindow& wnd, all_controls& control, map& map_) {

	if (control.redactor_mode) return;

	if (control.choose_artillery == artillery::_ballista) {

		if (key_is(key::Down)) ballista_ -= 2;

		else if (key_is(key::Up)) ballista_ += 2;

		else if (key_is(key::Space)) {

			ballista_.attack(wnd, control, map_);
			control.win_or_lose = true;

		}

		control.repaint = true;

	}

	else if (control.choose_artillery == artillery::_catapult) {

		if (key_is(key::Down)) catapult_ += 2;

		else if (key_is(key::Up)) catapult_ -= 2;

		else if (key_is(key::Left)) catapult_.move(-2);

		else if (key_is(key::Right)) catapult_.move(2);

		else if (key_is(key::Space)) {

			attack_artillery(wnd, catapult_, control, map_);
			control.win_or_lose = true;

		}

		control.repaint = true;

	}

	else if (control.choose_artillery == artillery::_trebuchet) {

		if (key_is(key::Down)) trebuchet_ += 2;

		else if (key_is(key::Up)) trebuchet_ -= 2;

		else if (key_is(key::Space)) {

			attack_artillery(wnd, trebuchet_, control, map_);
			control.win_or_lose = true;

		}
		control.repaint = true;

	}

	if (control.repaint) { 
		
		show(wnd, control, map_); 
		control.time = clock();
		control.repaint = false;

	}

	if (control.win_or_lose) {

		if (!map_.get_for_wins())cout << "you win!\n";

		else if (!ballista_.get_missiles() && !catapult_.get_missiles() && !trebuchet_.get_missiles())
			cout << "you lose\n";

		control.win_or_lose = false;

	}

}

void show_artillery(RenderWindow& wnd, artillery& art) { art.show(wnd); }

void attack_artillery(RenderWindow& wnd, artillery& art,all_controls& control, map& map_) { art.attack(wnd,control,map_); }

void help_me() {

	std::cout << "|||function help|||\n";

	std::cout << "r - redactor mode\nf - control frame\nspace - attack\n1 - ballista | 2 - catapult | 3 - trebuchet\n";
	std::cout << "d - destroy mode\nu - unlimited patrons mode\n";
	std::cout << "ballista - key top - aiming up | key bottom - aiming bottom\n";
	std::cout << "catapult - key top - aiming up | key bottom - aiming bottom | key left - move left | key right - move right\n";
	std::cout << "trebuchet - key top - aiming up | key bottom - aiming bottom\n";

}