#pragma once
#include <iostream>
#include <SFML/Graphics.hpp>

namespace standart_std {

	using namespace sf;
	using std::endl;
	using std::cout;
	using std::fstream;
	using std::ios;
	using std::string;

}

class interface_{
	
	sf::Texture texture_frame;

	sf::Sprite frame;

public:

	interface_();
	
	void show(sf::RenderWindow&);

};
	
struct all_controls {

	sf::Event evt;
	sf::Mouse mouse;
	interface_ frame;

	int time;
	unsigned short choose_artillery;
	int choose_block;
	bool redactor_mode;
	bool repaint;
	bool frame_icon;
	bool new_missiles;
	bool win_or_lose;

	all_controls();

};

void help_me();