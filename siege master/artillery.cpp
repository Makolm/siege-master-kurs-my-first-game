#include "SFML/Graphics.hpp"
#include "artillery.h"
#include "settings.h"
#include "controls.h"
#include <cmath>

#define M_PI       3.14159265358979323846

using namespace standart_std;

#define half 0.5f
#define speed_fall 9.8
#define radian M_PI/180

bool artillery::unlimited_patrons = false;

artillery::artillery(float x_, float y_) {  
	
	x = x_;
	y = y_;
	origin_lever = 31;
	min_rotate = speed = rotate = 0;
	max_rotate = 90;
	attack_power = missiles = 0;

	if (!texture_lever.loadFromFile("gallery/lever.png"))throw "error a_lever";
	
	lever.setTexture(texture_lever);
	
	lever.setOrigin(origin_lever, origin_lever);
	
	lever.setScale(0.3f, 0.3f);

}

bool artillery::is_air(map& map_, formul& formul_) {

	static bool hit = false;

	if (map_.get_block(formul_.block_x1, formul_.block_y1) == grass || map_.get_block(formul_.block_x1, formul_.block_y2) == grass)return false;
	else if (map_.get_block(formul_.block_x2, formul_.block_y1) == grass || map_.get_block(formul_.block_x2, formul_.block_y2) == grass)return false;

	else if (map_.get_block(formul_.block_x1, formul_.block_y3) == brick) {

		if (map_.get_mode())map_.get_hp_block(formul_.block_x1, formul_.block_y3) -= attack_power;
		return false;

	}

	else if (map_.get_block(formul_.block_x1, formul_.block_y1) == brick) {

		if (map_.get_mode())map_.get_hp_block(formul_.block_x1, formul_.block_y1) -= attack_power;
		return false;

	}

	else if (map_.get_block(formul_.block_x1, formul_.block_y2) == brick) {

		if(map_.get_mode())map_.get_hp_block(formul_.block_x1, formul_.block_y2) -= attack_power;
		return false;

	}

	if (!control_back(map_, formul_))return false;

	if (control_back_hit(map_, formul_))hit = true;

	else if (!hit && (map_.get_block(formul_.block_x1, formul_.block_y1) == target || map_.get_block(formul_.block_x1, formul_.block_y1) == target_reverse)) {

		map_.set_block(formul_.block_x1, formul_.block_y1, air);
		hit = true;

	}

	else if (!hit && (map_.get_block(formul_.block_x1, formul_.block_y2) == target || map_.get_block(formul_.block_x1, formul_.block_y2) == target_reverse)) {

		map_.set_block(formul_.block_x1, formul_.block_y2, air);
		hit = true;

	}

	if (hit) {

		map_--;
		hit = false;

		return false;
		
	}

	return true;

}

near_field_artillery::near_field_artillery(float x_, float y_) :artillery(x_, y_) { ; }

void near_field_artillery::attack(RenderWindow& wnd, all_controls& control, map& map_) { 

	if (!missiles && !unlimited_patrons) return;

	bool is_air = true;

	formul formul_;

	on_attack(formul_);
	
	while (is_air) {

		if ((control.time + 1) > clock())continue;

		is_air = pos_arrow(wnd,map_, formul_);

			float sin_ = (map::block_size - speed) * sin(rotate * radian), cos_ = (map::block_size - speed) * cos(rotate * radian);;
		if(is_air)arrow.setPosition(formul_.coord.x + formul_.x+formul_.w_h.width-cos_, formul_.coord.y - formul_.y + formul_.w_h.height/2+sin_);

		else {


			arrow.setPosition(formul_.coord.x + formul_.x + formul_.w_h.width - cos_, formul_.coord.y - formul_.y + formul_.w_h.height / 2 + sin_);

		}

		//CircleShape p1(2), p2(2), p3(3);
		//p1.setFillColor(Color::Blue); p2.setFillColor(Color::Red); p3.setFillColor(Color::Green);

		//p1.setPosition(formul_.coord.x + formul_.x + formul_.w_h.width, formul_.coord.y - formul_.y + formul_.w_h.height / 2);
		//p2.setPosition(formul_.coord.x + formul_.x + formul_.w_h.width, formul_.coord.y - formul_.y + formul_.w_h.height);
		//p3.setPosition(formul_.coord.x + formul_.x + formul_.w_h.width, formul_.coord.y - formul_.y);
		//
		//wnd.draw(p1); wnd.draw(p2); wnd.draw(p3); wnd.display();

		::show(wnd, control, map_);

		cout << "x: " << formul_.block_x1 << endl;
		cout << "y: " << formul_.block_y1 << endl;

		if (formul_.block_x1 > map::width || formul_.block_y1 < 0) { cout << "The limit of map\n"; break; }

		control.time = clock();

	}

	while ((control.time + 250) > clock());

	if (!unlimited_patrons) missiles--;


	cout << map_.get_for_wins() << " target(s) left\n";
	if (!unlimited_patrons) cout << missiles << " arrow(s) left\n";
	else cout << "infinity arrows left\n";

	to_origin();
	

}

bool near_field_artillery::pos_arrow(RenderWindow& wnd, map& map_, formul& formul_) {

	formul_.x += speed * cos(rotate * radian);
	formul_.y += speed * sin(rotate * radian);

	formul_.block_x1 = (formul_.coord.x + formul_.x + formul_.w_h.width) / map::block_size;
	formul_.block_x2 = (formul_.coord.x + formul_.x) / map::block_size;
	formul_.block_y1 = ((double)formul_.coord.y + formul_.w_h.height - formul_.y) / map::block_size;
	formul_.block_y2 = (formul_.coord.y - formul_.y) / map::block_size;
	formul_.block_y3 = (formul_.coord.y - formul_.y + formul_.w_h.height / 2) / map::block_size;

	return is_air(map_,formul_);
}

ballista::ballista(float x_, float y_) :near_field_artillery(x_, y_) {

	speed = 5; max_rotate = 30; rotate = 0.f; min_rotate = -30; attack_power = 1;
	
	 sin_arrow = sin(rotate * radian), cos_arrow = cos(rotate * radian);

	if (!texture_base.loadFromFile("gallery/ballista/base.png")) throw "error b_base";
	else if (!texture_stand.loadFromFile("gallery/ballista/stand.png")) throw "error b_stand";
	else if (!texture_arrow.loadFromFile("gallery/ballista/arrow.png")) throw "error b_arrow";

	base.setTexture(texture_base);
	stand.setTexture(texture_stand);
	arrow.setTexture(texture_arrow);

	base.setOrigin(base_origin_x, base_origin_y+half);

	base.setPosition(x+base_x, y+base_y);
	stand.setPosition(x + stand_x, y + stand_y);
	arrow.setPosition(x+arrow_x, y+arrow_y);
	lever.setPosition(x+lever_x, y+lever_y);
	
}

void ballista::show(RenderWindow& wnd) {

	wnd.draw(stand);
	if(missiles || unlimited_patrons)wnd.draw(arrow);
	wnd.draw(base);
	wnd.draw(lever);

}

void ballista::on_attack(formul& formul_) {

	formul_.coord = arrow.getPosition();
	formul_.w_h = arrow.getTextureRect();	

}

void ballista::to_origin() {

	arrow.setPosition(x + arrow_x + cos_arrow / 1.5, y + arrow_y - sin_arrow / 1.5);

}

ballista& ballista::aiming_at(float angle) {

	if ((rotate + angle) >= max_rotate || (rotate + angle) <= min_rotate)return *this;

	rotate += angle;
	base.rotate(-angle);
	arrow.rotate(-angle);
	lever.rotate(-angle*2);

	float sub = sin((90. + rotate) * radian), sub2 = cos((90.+rotate)*radian);
	sin_arrow+= (angle >= 0) ? sub : sub * -1;
	cos_arrow += (angle >= 0) ? sub2 : sub2 * -1;

	std::cout << 90-rotate << endl;

	arrow.setPosition(x + arrow_x+cos_arrow/1.5, y + arrow_y - sin_arrow/1.5);

	return *this;

}

ballista& ballista::operator+=(float angle) { aiming_at(angle); return *this; }

ballista& ballista::operator-= (float angle) { aiming_at(-angle); return *this; }

far_field_artillery::far_field_artillery(float x_, float y_) :artillery(x_, y_) { 

	min_rotate = rotate = 20;

}

void far_field_artillery::attack(RenderWindow& wnd, all_controls& control,map& map_) {

	if (!missiles && !unlimited_patrons) return;

	//  ����� ����������/�������� ���� ������� ������
	if (rotate <= min_rotate) { cout << "rotate must have more than " << min_rotate/2 << " degree\n"; return; } 

		formul formul_;

		formul_.degree = rotate / 2;

		formul_.w_h = stone.getTextureRect();

		while (rotate>0) {

			if ((control.time + 10) > clock())continue;

			aiming_at(-2);

			::show(wnd, control, map_);

			control.time = clock();

		}

		on_attack(formul_);

		bool is_air = true;
	
		while(is_air) {

			if ((control.time + 25) > clock())continue;

			is_air = pos_stone(map_,formul_);
			
			stone.setPosition((float)formul_.x+formul_.coord.x, (float)formul_.coord.y-formul_.y);

			std::cout << "x: " << formul_.block_x1 << endl;
			std::cout << "y: " << formul_.block_y1 << endl;

			control.time = clock();

			::show(wnd,control,map_);

			if (formul_.block_x1 > map::width || formul_.block_y1 < 0) { cout << "The limit of map\n"; break; }

		}

		if (!unlimited_patrons) missiles--;

		cout << map_.get_for_wins() << " target(s) left\n";
		if (!unlimited_patrons) cout << missiles << " stone(s) left\n";
		else cout << "infinity stones left\n";

		to_origin();
		
}

bool far_field_artillery::pos_stone(map& map_, formul& formul_) {

	formul_.y = formul_.x * tan(formul_.degree * radian) - speed_fall / (2 * (double)speed * speed * cos(formul_.degree * radian) * cos(formul_.degree * radian)) * formul_.x * formul_.x;

	formul_.x += 15;

	formul_.block_x1 = (formul_.coord.x + formul_.x + formul_.w_h.width) / 32;
	formul_.block_x2 = (formul_.coord.x+formul_.x)/32 ;
	formul_.block_y1 = ((double)formul_.coord.y + formul_.w_h.height - formul_.y) / 32;
	formul_.block_y2 = (formul_.coord.y - formul_.y) / 32;

	return is_air(map_,formul_);

}

bool far_field_artillery::control_back(map& map_, formul& formul_) {

	if (map_.get_block(formul_.block_x2, formul_.block_y1) == brick) {

		if(map_.get_mode())map_.get_hp_block(formul_.block_x2, formul_.block_y1) -= attack_power;
		return false;

	}

	else if (map_.get_block(formul_.block_x2, formul_.block_y2) == brick) {

		if(map_.get_mode())map_.get_hp_block(formul_.block_x2, formul_.block_y2) -=attack_power;
		return false;

	}

	return true;

}

bool far_field_artillery::control_back_hit(map& map_, formul& formul_) {

	if (map_.get_block(formul_.block_x2, formul_.block_y1) == target || map_.get_block(formul_.block_x2, formul_.block_y1) == target_reverse) {
	
		map_.set_block(formul_.block_x2, formul_.block_y1, air);
		return true;

	}

	else if (map_.get_block(formul_.block_x2, formul_.block_y2) == target || map_.get_block(formul_.block_x2, formul_.block_y2) == target_reverse) {

		map_.set_block(formul_.block_x2, formul_.block_y2, air);
		return true;

	}

	return false;

}

void far_field_artillery::aiming_at(float angle) {

	if ((rotate + angle) < 0) { cout << "this is min aiming\n"; return; }

	else if ((rotate + angle) > max_rotate) { cout << "this is max aiming\n"; return; }

	rotate += angle;

	sprites(angle);

	std::cout << rotate/2 << endl;

}

catapult::catapult(float x_, float y_) :far_field_artillery(x_, y_) {

	distation = 0; speed = 100;  max_distation = 70; max_rotate = 80; attack_power = 2;

	if (!texture_base.loadFromFile("gallery/catapult/base.png"))throw "error c_base";
	else if (!texture_dipper.loadFromFile("gallery/catapult/dipper.png"))throw "error c_dipper";
	else if (!texture_wheel.loadFromFile("gallery/catapult/wheel.png"))throw "error c_wheel";
	else if (!texture_stone.loadFromFile("gallery/catapult/stone.png"))throw "error c_stone";

	base.setTexture(texture_base);
	dipper.setTexture(texture_dipper);
	wheel_1.setTexture(texture_wheel);
	wheel_2.setTexture(texture_wheel);
	stone.setTexture(texture_stone);

	dipper.setOrigin(origin_dipper_x, origin_dipper_y);
	wheel_1.setOrigin(wheel_origin, wheel_origin);
	wheel_2.setOrigin(wheel_origin, wheel_origin);
	stone.setOrigin(origin_dipper_x, origin_dipper_y);

	base.setPosition(x, y + base_y);
	dipper.setPosition(x + dipper_x, y + dipper_y);
	wheel_1.setPosition(x + wheel_1_x, y + wheel_y);
	wheel_2.setPosition(x + wheel_2_x, y + wheel_y);
	stone.setPosition(x+stone_x, y+stone_y);

	lever.setPosition(x + lever_x, y + lever_y);

}

void catapult::show(RenderWindow& wnd) {

	wnd.draw(dipper);
	wnd.draw(base);
	wnd.draw(lever);
	wnd.draw(wheel_1);
	wnd.draw(wheel_2);
	if (missiles || unlimited_patrons)wnd.draw(stone);

}

void catapult::sprites(float angle) {

	lever.rotate(-angle);
	dipper.rotate(-angle);
	stone.rotate(-angle);

}

void catapult::on_attack(formul& formul_) {

	stone.setRotation(0);
	stone.setOrigin(0, 0);
	stone.setPosition(x + attack_x, y + attack_y);

	formul_.coord = stone.getPosition();

}

void catapult::to_origin() { 
	
	stone.setOrigin(origin_dipper_x,origin_dipper_y);
	stone.setPosition(x + stone_x,y + stone_y); 

	dipper.rotate(-min_rotate);
	lever.rotate(-min_rotate);
	rotate = min_rotate;

}

catapult& catapult::move(float to) {

	if ((distation + to) < 0) { cout << "this is min distation of catapult\n"; return *this; }

	else if ((distation + to) > max_distation) { cout << "this is max distation of catapult\n"; return *this; }

	wheel_1.rotate(to); wheel_2.rotate(to);

	x += to; distation += to;

	return go_in(x,y);

}

catapult& catapult::go_in(float x_, float y_) {

	base.setPosition(x_, y_ + base_y);
	dipper.setPosition(x_+dipper_x, y_+dipper_y);
	wheel_1.setPosition(x_+wheel_1_x, y_+wheel_y);
	wheel_2.setPosition(x_+wheel_2_x, y_+wheel_y);
	lever.setPosition(x_+lever_x, y_+lever_y);
	stone.setPosition(x_ + stone_x, y_ + stone_y);

	return *this;

}

catapult& catapult::set_max_distation(float dist) {

	if (dist < 0)return *this;

	max_distation = dist;

	return *this;

}

catapult& catapult::operator+=(float angle) { aiming_at(angle); return *this; }

catapult& catapult::operator-=(float angle) { aiming_at(-angle); return *this; }

trebuchet::trebuchet(float x_, float y_) :far_field_artillery(x_, y_) {
	
	min_rotate = rotate = 50; speed = 220; max_rotate = 80; attack_power = 3;

	if (!texture_base.loadFromFile("gallery/trebuchet/base.png"))throw "error t_base";
	else if (!texture_dipper.loadFromFile("gallery/trebuchet/dipper.png"))throw "error t_dipper";
	else if (!texture_stone.loadFromFile("gallery/trebuchet/stone.png")) throw "error t_stone";

	base.setTexture(texture_base);
	dipper.setTexture(texture_dipper);
	stone.setTexture(texture_stone);

	dipper.setOrigin(dipper_origin_x+half, dipper_origin_y);
	stone.setOrigin(dipper_origin_x + half, dipper_origin_y);

	base.setPosition(x, y+base_y);
	dipper.setPosition(x+dipper_x, y+dipper_y);
	lever.setPosition(x+lever_x, y+lever_y);
	stone.setPosition(x+stone_x, y+stone_y);

}

void trebuchet::show(RenderWindow& wnd) {

	wnd.draw(dipper);
	wnd.draw(base);
	wnd.draw(lever);
	if (missiles || unlimited_patrons)wnd.draw(stone);

}

void trebuchet::sprites(float angle){

	lever.rotate(-angle);
	dipper.rotate(-angle);
	stone.rotate(-angle);

}

void trebuchet::on_attack(formul& formul_) {

	stone.setRotation(0);
	stone.setOrigin(0.f,0.f);
	stone.setPosition(x+attack_x, y+attack_y);

	formul_.coord = stone.getPosition();

}

void trebuchet::to_origin() {

	stone.setOrigin(dipper_origin_x + half, dipper_origin_y);
	stone.setPosition(x + stone_x, y + stone_y);

	dipper.rotate(-min_rotate);
	lever.rotate(-min_rotate);
	rotate = min_rotate;

}

trebuchet& trebuchet::operator+=(float angle) { aiming_at(angle); return* this; }

trebuchet& trebuchet::operator-=(float angle) { aiming_at(-angle); return *this; }