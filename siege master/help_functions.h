#pragma once

namespace hf {

	double extent(double, unsigned);

	unsigned fact(unsigned);

	double sinus(double, unsigned);

}