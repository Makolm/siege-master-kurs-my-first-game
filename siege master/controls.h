#pragma once
#include <SFML/Graphics.hpp>
#include "map.h"
#include "artillery.h"
#include "settings.h"

using namespace standart_std;

void show(RenderWindow&, all_controls&, map&);

void control_events(RenderWindow&, all_controls&, map&);

void all_animations(RenderWindow&, all_controls&, map&);

void show_artillery(RenderWindow&, artillery& art);

void attack_artillery(RenderWindow&, artillery&, all_controls&, map&);