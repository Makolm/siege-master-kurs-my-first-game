#include "map.h"
#include <SFML/Graphics.hpp>
#include "settings.h"

using namespace standart_std;

typedef unsigned long long ull;

map::map() {

	file_save = "levels/level_1.dat";

	txr.loadFromFile("gallery/texture_pack_test.png");
	spr.setTexture(txr);

	blocks = new element[(ull)width * height];
	rows = new element*[height];
	
	for (unsigned i = 0; i < width * height; i++) {

		blocks[i].block = air;
		blocks[i].hp = hp_air;

	}

	for (unsigned i = 0; i < height; i++)
		rows[i] = blocks + i * (ull)width;

	destroy_mode = need_recount = true;
	need_for_wins = 0;

}

void map::draw(RenderWindow& window) {

	 for(unsigned i =0; i< height; i++)
		 for (unsigned j = 0; j < width; j++) {

			 if (rows[i][j].block == air)
				 spr.setTextureRect(IntRect(4 * block_size, 0 * block_size, block_size, block_size));

			 else if (rows[i][j].block == dirt)
				 spr.setTextureRect(IntRect(1 * block_size, 0 * block_size, block_size, block_size));

			 else if(rows[i][j].block == grass)
				 spr.setTextureRect(IntRect(0 * block_size, 0 * block_size, block_size, block_size));

			 else if(rows[i][j].block == brick && rows[i][j].hp>=3)
				 spr.setTextureRect(IntRect(3 * block_size, 1 * block_size, block_size, block_size));

			 else if(rows[i][j].block == brick && rows[i][j].hp == 2)
				 spr.setTextureRect(IntRect(4 * block_size, 1 * block_size, block_size, block_size));

			 else if(rows[i][j].block == brick && rows[i][j].hp == 1)
				 spr.setTextureRect(IntRect(5 * block_size, 1 * block_size, block_size, block_size));

			 else if (rows[i][j].block == brick && rows[i][j].hp <= 0) {

				 rows[i][j].block = air; j--;
				 continue;

			 }

			 else if (rows[i][j].block == target) {

				 spr.setTextureRect(IntRect(18 * block_size, 0 * block_size, block_size, block_size));
				 
				 if(need_recount)need_for_wins++;

			 }

			 else if (rows[i][j].block == target_reverse) {
			
				 spr.setTextureRect(IntRect(19 * block_size, 0 * block_size, block_size, block_size));
				 
				 if(need_recount)need_for_wins++;

			 }

			 spr.setPosition(j * block_size * 1.f, i * block_size * 1.f);

			 window.draw(spr);

		 }

	 if (need_recount) { need_recount = false; }

}

void map::redactor(RenderWindow& window, all_controls& controls) {

	unsigned x = ((int)controls.mouse.getPosition(window).x)/block_size;
	unsigned y = ((int)controls.mouse.getPosition(window).y)/block_size;
	
	if (x >= 0 && x < width && y >= 0 && y < height) {

		rows[y][x].block = controls.choose_block;

		if (controls.choose_block == brick)rows[y][x].hp = hp_brick;

	}

}

int map::get_block(unsigned x_, unsigned y_) {

	if (x_  >= width || y_ >= height)return -1;

	return rows[y_][x_].block;

}

int& map::get_hp_block(unsigned x_, unsigned y_) {

	if (x_ >= width || y_ >= height)return rows[0][0].hp;

	return rows[y_][x_].hp;

}

void map::set_block(unsigned x_, unsigned y_, block block_) {

	if (x_ >= width || y_ >= height)return;

	rows[y_][x_].block = block_;
	rows[y_][x_].hp = block_;

}

void map::need_restart() { need_recount = true; need_for_wins = 0; }

map::~map() { delete[] blocks; }

interface_::interface_(){

	if(!texture_frame.loadFromFile("gallery/Frame.png"))throw "error interface";

	frame.setTexture(texture_frame);

	IntRect sub = frame.getTextureRect();

	frame.setPosition((map::width*map::block_size-sub.width) / 2.f, 0.f);

}

void interface_::show(RenderWindow& wnd) { wnd.draw(frame); }