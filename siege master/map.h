#pragma once

#include <SFML/Graphics.hpp>
#include <fstream>
#include "settings.h"

using namespace standart_std;

enum block {

	air,
	dirt,
	grass,
	brick,
	target,
	target_reverse,

};

enum hp_block {

	hp_air = 0,
	hp_dirt = 0,
	hp_grass = 0,
	hp_brick = 3,
	hp_target = 1,

};

class map {

	string file_save;

	fstream file;

	Texture txr;
	Sprite spr;

	struct element {

		int hp;
		char block;

		void operator -= (unsigned hp_) { hp -= hp_; }

	};

	element* blocks;
	element** rows;

	bool need_recount,destroy_mode;
	unsigned need_for_wins;

public:

	enum map_character {

		block_size = 32,
		height = 24,// 768 / 32 
		width = 40// 1280 / 32

	};

	map();

	void draw(RenderWindow&);

	void redactor(RenderWindow&, all_controls&);

	unsigned get_width() { return width; }

	unsigned get_height() { return height; }

	int get_block(unsigned,unsigned);

	int& get_hp_block(unsigned, unsigned);

	void set_block(unsigned, unsigned, block);

	unsigned get_for_wins() { return need_for_wins; }

	void need_restart();

	bool get_mode() { return destroy_mode; }

	void set_mode(bool mode) { destroy_mode = mode; }

	void save();
	
	void load();

	void operator --(int) { need_for_wins--; }

	~map();

};