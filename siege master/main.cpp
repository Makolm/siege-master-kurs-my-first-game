#include <SFML/Graphics.hpp>
#include "map.h"
#include "artillery.h"
#include "controls.h"
#include "settings.h"

using namespace standart_std;

void main(){

	cout << "h - function help\n";

	RenderWindow wnd(VideoMode(1280, 768), "siege master");

	all_controls control;

	map map_; map_.load();

	show(wnd,control,map_);

	while (wnd.isOpen()) {

		while (wnd.pollEvent(control.evt)) {
		
			control_events(wnd, control, map_);
			all_animations(wnd, control, map_);

		}
	}

}