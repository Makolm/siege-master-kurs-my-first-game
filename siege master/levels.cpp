#include "map.h"
#include "settings.h"
#include <fstream>

using namespace standart_std;

void map::load() {

	file.open(file_save.c_str(), ios::in | ios::binary);

	if (!file) {

		file.close();
		throw "Error file read";

	}

	file.read((char*)blocks, sizeof(element) * height * width);

	need_restart();

	file.close();

}

void map::save() {

	file.open(file_save.c_str(), ios::out | ios::binary);

	file.write((const char*)blocks, sizeof(element) * height * width);

	file.close();

}